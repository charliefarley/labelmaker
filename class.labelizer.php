<?php

require_once('tcpdf/tcpdf.php');



	// $pdf = new TCPDF();

	class Labelizer extends TCPDF {

		protected $columns = 2;
		protected $rows = 4;
		protected $cropMarks = false;
		protected $lines = array();

		public function __construct() {
			parent::__construct();
		}

		public function setColumns($columns) {
			$this->columns = $columns;
		}
		public function getColumns() {
			return $this->columns;
		}

		public function setCropMarks($bool) {
			$this->cropMarks = $bool;
		}

		public function getCropMarks() {
			return $this->cropMarks;
		}

		public function setRows($rows) {
			$this->rows = $rows;
		}
		public function getRows() {
			return $this->rows;
		}

		/**
		 * Adds a text line to the label
		 * @param $text (string) The text for the line
		 * @param $size (int) Font size in points
		 * @param $margin_bottom (int) The margin under the text in points
		 */
		public function setLine($text, $size, $margin_bottom) {
			$this->lines[] = array(
				'text' => (string) $text,
				'size' => (int) $size,
				'margin_bottom' => (int) $margin_bottom
			);
		}
		public function getLines() {
			return $this->lines;
		}

		public function setBeerName($beer_name) {
			$this->beer_name = $beer_name;
		}
		public function getBeerName() {
			return $this->beer_name;
		}

		public function setStrapline($strapline) {
			$this->strapline = $strapline;
		}
		public function getStrapline() {
			return $this->strapline;
		}

		public function setAbv($abv) {
			$this->abv = $abv;
		}
		public function getAbv() {
			return $this->abv;
		}

		public function setIbu($ibu) {
			$this->ibu = $ibu;
		}
		public function getIbu() {
			return $this->ibu;
		}

		public function setDateBrewed($date_brewed) {
			$this->date_brewed = $date_brewed;
		}
		public function getDateBrewed() {
			return $this->date_brewed;
		}

		public function setDateBottled($date_bottled) {
			$this->date_bottled = $date_bottled;
		}
		public function getDateBottled() {
			return $this->date_bottled;
		}

		public function AddPage($orientation='', $format='', $keepmargins=false, $tocpage=false) {
			$this->_setUp();
			parent::AddPage();
		}

		private function _setUp() {
			$page_dimensions = $this->getPageDimensions();
			$this->margins = $this->getMargins();
			$this->page_width = floor($page_dimensions['wk'] - $this->margins['left'] - $this->margins['right']);
			$this->page_height = floor($page_dimensions['hk'] - $this->margins['top'] - $this->margins['bottom']);		
			$this->row_height = floor($this->page_height / $this->getRows());
			$this->column_width = floor($this->page_width / $this->getColumns());
		}

		private function _createCropMarks() {
			$top = $this->margins['top'];

			for ($row = 0; $row < $this->getRows(); $row++) {

				// $add_margin = ($row == 0) ? $this->margins['top'] : 0;
				$top = floor($row * $this->row_height) + $this->margins['top'];

				$left = $this->margins['left'];

				for ($col = 0; $col < $this->getColumns(); $col++) {

					// $add_margin = ($col == 0) ? $this->margins['left'] : 0;
					$left = floor($this->column_width * $col) + $this->margins['left'];

					$this->cropMark($left, $top, 3, 3, 'A,D', array(100,100,100));

					if ($col == ($this->getColumns() -1)) {
						$this->cropMark(($left + $this->column_width), $top, 3, 3, 'A,D', array(100,100,100));
					}
					if ($row == ($this->getRows() -1)) {
						$this->cropMark($left, ($top + $this->row_height), 3, 3, 'A,D', array(100,100,100));
						if ($col == ($this->getColumns() -1)) {
							$this->cropMark(($left + $this->column_width), ($top + $this->row_height), 3, 3, 'A,D', array(100,100,100));
						}
					}
				}
			}
		}

		private function _addLabel($x, $y) {
			// MultiCell($w, $h, $txt, $border=0, $align='J', $fill=false, $ln=1, $x='', $y='')
			// $x = ($this->column_width + $this->margins['left']);
			// $y = ($this->row_height + $this->margins['top']);
			// $this->SetFontSize(20);
			// $this->MultiCell($this->column_width, 0, $this->getBeerName(), 0, 'C', false, 1, $x, $y);
			// $this->SetFontSize(10);
			// $this->MultiCell($this->column_width, 0, $this->getStrapline(), 0, 'C', false, 1, $x);
			// $this->SetFontSize(14);
			// $this->MultiCell($this->column_width, 0, $this->getAbv().'%', 0, 'C', false, 1, $x);
			// $this->MultiCell($this->column_width, 0, $this->getIbu().' IBU', 0, 'C', false, 1, $x);
			// $this->SetFontSize(10);
			// $this->MultiCell($this->column_width, 0, 'Brewed '.$this->getDateBrewed(), 0, 'C', false, 1, $x);
			// $this->MultiCell($this->column_width, 0, 'Bottled '.$this->getDateBottled(), 0, 'C', false, 1, $x);

			$lines = $this->getLines();

			// var_dump($lines);
			$i = 0;
			foreach ($lines as $line) {
				
				if ($i == 0) {
					$this->SetFont('tradegothicbwebfont');
				} else {
					$this->SetFont('tradegothicwebfont');
				}

				if ($line['size'] !== -1) {
					$this->SetFontSize($line['size']);
				}
				$y = ($i == 0) ? $y : '';
				$this->MultiCell($this->column_width, 0, $line['text'], 0, 'C', false, 1, $x, $y);
				if ($line['margin_bottom'] !== -1) {
					$this->Ln($line['margin_bottom']);
				}
				$i++;
			}

		}

		public function createLabels() {
			
			if ($this->cropMarks) $this->_createCropMarks();
			

			for ($row = 0; $row < $this->getRows(); $row++) {

				$y = floor($row * $this->row_height) + $this->margins['top'];

				$x = $this->margins['left'];

				for ($col = 0; $col < $this->getColumns(); $col++) {
					$x = floor($this->column_width * $col) + $this->margins['left'];

					$this->_addLabel($x, $y);

				}

			}
		}


		
	}