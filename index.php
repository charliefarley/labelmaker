<?php
if ( ! empty($_POST)) {

	require_once('class.labelizer.php');

	$pdf = new Labelizer();

	$pdf->setColumns(3);
	$pdf->setRows(5);


	// var_dump($_POST);


	for($i=0; $i<count($_POST['text']); $i++) {
		$pdf->setLine($_POST['text'][$i], $_POST['size'][$i], $_POST['margin_bottom'][$i]);
	}

	if (isset($_POST['crop_marks'])) $pdf->setCropMarks(true);
	// set document information
	$pdf->SetCreator(PDF_CREATOR);
	$pdf->SetAuthor('Labelizer');
	
	

	// remove default header/footer
	$pdf->setPrintHeader(false);
	$pdf->setPrintFooter(false);


	$pdf->AddPage();

	$pdf->SetFont('helvetica', '', 12);

	$pdf->createLabels();

// var_dump($_POST);

	$filename = str_replace(' ', '', strtolower($_POST['line'][0]));

	// save config as json file
	$config = json_encode(array('data' => $_POST));
	
// var_dump($config);

	file_put_contents('configs/'.$filename.'.json', $config);

	// output pdf
	$pdf->Output($filename.'.pdf', 'D');

}


if (isset($_GET['file'])) {
	$file = file_get_contents('configs/'.$_GET['file']);
	$config = json_decode($file);

	var_dump($config);
}
?>
<!DOCTYPE html>
<html lang="en">
<head>
	<title>Beer Labelizer</title>
</head>
<body>


<h1>Labelizer</h1>

<form method="post" target="_blank">
	
	<a href="open.php">Open a saved file</a>

	<div><input type="text" name="columns" id="columns" value="<?php if (isset($config->data->columns)) { echo $config->data->columns; } else { echo 3; } ?>"><label for="columns">Columns</label></div>
	<div><input type="text" name="rows" id="rows" value="<?php if (isset($config->data->rows)) { echo $config->data->rows; } else { echo 5; } ?>"><label for="rows">Rows</label></div>

<!-- 	<input type="text" name="beer_name" placeholder="Brew Name" placeholder="Brew Name">
	<input type="text" name="strapline" placeholder="Strapline">
	<input type="text" name="abv" placeholder="ABV">
	<input type="text" name="ibu" placeholder="IBU">
	<input type="text" name="date_brewed" placeholder="Date Brewed">
	<input type="text" name="date_bottled" placeholder="Date Bottled"> -->
	
	<table class="lines">
		<tr>
			<th>Text</th>
			<th>Font Size</th>
			<th>Margin</th>
			<th>Remove</th>
		</tr>
		<?php if (isset($config)) {
			for($i=0; $i<count($config->data->text); $i++) { ?>


			<tr class="line">	
				<td>
					<input type="text" name="text[]" value="<?php echo $config->data->text[$i]; ?>">
				</td>
				<td>
					<select name="size[]">
						<option value="-1">Font Size</option>
						<?php for ($i=8; $i<33; $i++) { ?>
						<?php $sel = ($i == $config->data->size[$i]) ? ' selected':''; ?>
						<option<?php echo $sel; ?> value="<?php echo $i; ?>"><?php echo $i; ?></option>
						<?php } ?>
					</select>
				</td>
				<td>
					<select name="margin_bottom[]">
						<option value="-1">Margin</option>
						<?php for ($i=0; $i<10; $i++) { ?>
						<?php $sel = ($i == $config->data->margin_bottom[$i]) ? ' selected':''; ?>				
						<option<?php echo $sel; ?> value="<?php echo $i; ?>"><?php echo $i; ?></option>
						<?php } ?>
					</select>
				</td>
				<td>
					<button class="remove">X</button>
				</td>
			</tr>

		<?php }
		} else { ?>
		<tr class="line">	
			<td>
				<input type="text" name="text[]">
			</td>
			<td>
				<select name="size[]">
					<option value="-1">Font Size</option>
					<?php for ($i=8; $i<33; $i++) { ?>
					<option value="<?php echo $i; ?>"><?php echo $i; ?></option>
					<?php } ?>
				</select>
			</td>
			<td>
				<select name="margin_bottom[]">
					<option value="-1">Margin</option>
					<?php for ($i=0; $i<10; $i++) { ?>
					<option value="<?php echo $i; ?>"><?php echo $i; ?></option>
					<?php } ?>
				</select>
			</td>
			<td>
				<button class="remove">X</button>
			</td>
		</tr>
		<?php } ?>
	</table>
	<button class="add">Add Line</button>
	<div>
		<input type="checkbox" name="crop_marks" id="crop_marks"><label for="crop_marks">Crop Marks?</label>
	</div>
	<input type="submit" value="Create Labels">

</form>



<script src="//ajax.googleapis.com/ajax/libs/jquery/2.0.3/jquery.min.js"></script>
<script>
$(document).ready(function() {
	$('.add').on('click', function(e) {
		e.preventDefault();
		var clone = $('.line').last().clone();//.clone();
		// console.log(clone);
		$('.lines').append(clone);
	});

	$('.lines').on('click', '.remove', function(e) {
		e.preventDefault();
		$(this).parent().remove();
	});
});
</script>
</body>
</html>